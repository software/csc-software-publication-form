TEX=pubsoftform.tex
PDF=pubsoftform.odf

$(PDF): $(TEX) pubsoftform.cls 
	latexmk -pdf $<

.PHONY: clean
clean:
	latexmk -C
