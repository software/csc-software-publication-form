This is a form for an approval of software publication.

The form is pretty much self-explaining. You have to edit
`pubsoftform.tex`. In the `example-pubsoftform.pdf` there are examples
given in each section. The defining bits of text of the sections are
contained in the class file `pubsoftform.cls` which must not be changed. 

Please see the related csc-wiki entry on [CSC Software Publications](http://wiki2/csc/index.php/CSC_Software_Publications).
