%% This is file "pubsoftform.cls" by Jens Saak
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% saak@mpi-magdeburg.mpg.de
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% You can use LATEX or PDFLATEX
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ProvidesClass{pubsoftform}[2018/07/11 v1.0 pubsoftform (saak)]
\NeedsTeXFormat{LaTeX2e}[1995/06/01]

% Inherit features of the Article class
\LoadClass{article}

% Define page gemometry and boundaries
\RequirePackage[a4paper,left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}

% enable hyperlinks and forms
\RequirePackage[colorlinks]{hyperref}

% provide multicolumn environment
\RequirePackage{multicol}

% Define some basic strings for later use in the single paragraphs
\def\mdl{\texttt{MODULE}}

\def\permistr{Hereby, I grant to \insertapplnames~the permission to publish
  \mdl~under the conditions defined in \S 1. and \S2. This permission extends to
minor updates as long as none of the conditions under \S1.--\S5. have changed
significantly, e.g. \mdl{} incorporates work of additional persons, or uses new
software dependencies that require a license update. }

\def\termstr{\S 1. \mdl~will be published under the following terms}

\def\tpsoftsuite{\S 1.1. \mdl~will be published as a part of the following third
  party software suite}

\def\titlestr{\centering\large Permission for publication of the software
  module\\ -- referred to as \mdl~in what follows\\ -- granted to
  \insertapplnames} 

\def\scopestr{\S 2. The scope of the published part of \mdl~is defined as
  follows --- including additionally published submodules or dependencies}

\def\developerstr{\S 3. The following additional MPI Magdeburg affiliated
  persons have also been actively involved in the development of \mdl. Active
  development includes but is not limited to: patches, testing, documentation,
  provision of prototype codes, etc. By their signature they confirm that they
  know about the considered publication of \mdl~in form and extent defined in \S
  1.\ and \S 2.\ and that they have no objections against the publication.  }

\def\affectedstr{\S 4. The following features of \mdl{} are based upon or
  comparable to the (unpublished) work/ideas of the corresponding MPI Magdeburg affiliated persons. By their signature they confirm
  that they know about the considered publication of \mdl~in form and extent
  defined in \S 1.\ and \S 2.\ and that they have no objections against the
  publication of the following features.}

\def\expertstr{\S 5. The following persons are assigned CSC internal experts on
  scientific software development. By their signature they confirm that they
  know about the considered publication of \mdl~in the form \S 1.\ and that they
  have no objections against the publication.}

\def\reviewerstr{\S 6. The following additional persons have agreed to review the
  code according to the rules discussed in the CSC tech talk on Jan. 28, 2021.

  This status of the review is:
  \reviewchkboxes{}
}

\def\applicstr{We, the applicants, confirm by our signature that
\begin{enumerate}
\item the data in \S 1.--\S 6.\ is correct and complete,
\item all wanted or unwanted contributions of other MPI Magdeburg affiliates are
  described in \S 4.\ and are clearly highlighted and acknowledged upon
  publication in the code documentation,
\item the considered publication does not violate any rights of third parties,
\item we are aware of all implications of the chosen license: \insertlicense
\item and that we are aware that any failure in observing or any incompleteness
  in the preceeding terms 1., 2., 3., any violation of the terms defined in \S
  1., any excess of the scope defined in \S 2.\ immediately revokes the granted
  permission.
\end{enumerate}
}

% A simple name plus date plus signature line
\newcommand{\signatureline}[1]{%
  {\makebox[.4\linewidth][l]{#1} \quad {\scriptsize date}\makebox[.15\linewidth]{\dotfill}~{\scriptsize sign.}
    \dotfill}\par}
% A short macro to use when a block is not applicable
\newcommand{\notapplicable}{Not applicable.\par}

% Macros adding applicants and inserting them in the right places 
\def\insertapplnames{}
\def\applboxcontent{}
\newcommand{\applicant}[1]{
  \ifx\insertapplnames\empty\appto\insertapplnames{#1}\else\appto\insertapplnames{, #1}\fi
  \appto\applboxcontent{\signatureline{#1}}
}

% Some helpers to automatically fill certain parts of the form, like placing
% urls 
\newcommand{\insertlicense}{}
\newcommand{\mylicense}[1]{\renewcommand{\insertlicense}{{\ttfamily #1}}}
\newcommand{\insertmycpyrghthldrs}{}
\newcommand{\mycopyrightholders}[1]{\renewcommand{\insertmycpyrghthldrs}{#1}}
\newcommand{\mymodulename}[1]{\def\mdname{\huge \texttt{#1}}}
\newcommand{\insertmyurl}{}
\newcommand{\myurl}[1]{\renewcommand{\insertmyurl}{#1}}
\newcommand{\inserttheirurl}{}
\newcommand{\theirurl}[1]{\renewcommand{\inserttheirurl}{#1}}
\newcommand{\insertmyrepo}{}
\newcommand{\myrepo}[1]{\renewcommand{\insertmyrepo}{#1}}
\newcommand{\inserttheirrepo}{}
\newcommand{\theirrepo}[1]{\renewcommand{\inserttheirrepo}{#1}}
\newcommand{\inserttheirname}{}
\newcommand{\suitename}[1]{\renewcommand{\inserttheirname}{#1}}
\newcommand{\inserttheirscope}{}
\newcommand{\generalscope}[1]{\renewcommand{\inserttheirscope}{#1}}
\newcommand{\inserttheirinst}{}
\newcommand{\maininstitute}[1]{\renewcommand{\inserttheirinst}{#1}}
\newcommand{\inserttheirlic}{}
\newcommand{\theirlicense}[1]{\renewcommand{\inserttheirlic}{{\ttfamily #1}}}

% do not print a column separation line
\setlength{\columnseprule}{0pt}

% enable framed boxes to break at pages
\RequirePackage{longfbox}

% Define the MPI color
\definecolor{mpiblue}{HTML}{33a5c3} % light blue green

% default behavior of paragraphs
\setlength{\parindent}{0pt}
\setlength{\parskip}{\medskipamount}

% default description label formatting
\renewcommand{\descriptionlabel}[1]{\hspace{\labelsep} #1}

% define the actualframed box with colored header
\newenvironment{mybox}[1][]{%
  \begin{longfbox}[padding=.01\linewidth,breakable=true]%
    \colorbox{mpiblue}{\parbox{.9875\linewidth}{\bfseries \color{white}#1}}%
    \par%
}{%
  \end{longfbox}%
}

%% Box environment definitions
\newenvironment{scope}{%
  \begin{mybox}[\scopestr]%
    \begin{description}%
}{%
    \end{description}%
  \end{mybox}%
}

\newenvironment{developers}{\begin{mybox}[\developerstr]}{\end{mybox}}

\newenvironment{reviewers}{\begin{mybox}[\reviewerstr]}{\end{mybox}}

% An updated item macro for the 'affected' box  
\newcommand{\featureitem}[1]{\item[Feature: #1]~\\[1ex]}
% And the corresponding environment
\newenvironment{affected}{%
  \begin{mybox}[\affectedstr]%
    \begin{description}%
}{%
    \end{description}%
  \end{mybox}%
}%

\newenvironment{licexpert}{\begin{mybox}[\expertstr]}{\end{mybox}}

\newenvironment{applicants}{\begin{mybox}[\applicstr]}{\end{mybox}}
% checkboxes for use in forms
\newcommand{\mycheckbox}{\CheckBox[width=1ex,height=1ex,bordercolor=0 0 0]{}}
 
% the checkbox list at the top of the sheet
\def\iniprtchckbxschecked{%
  \begin{Form}
    \renewcommand{\labelitemi}{\mycheckbox{}}
    \begin{itemize}
    \item{Versions ready for use}
    \item{Under development}
    \item{Binaries}
    \item{Source code}
    \end{itemize}
  \end{Form}
}

\def\reviewchkboxes{%
  \begin{multicols}{3}
    \begin{Form}
    \renewcommand{\labelitemi}{\mycheckbox{}}
    \begin{itemize}
    \item{initiated}
    \item{ongoing}
    \item{finished}
    \end{itemize}
    \end{Form}
  \end{multicols}
}

\newcommand{\partofsuite}{%
  \begin{mybox}[\tpsoftsuite]%
    \begin{description}%
    \item[Name:]\dotfill\inserttheirname%
    \item[General scope:]\dotfill\inserttheirscope%
    \item[Website:]\dotfill\url{\inserttheirurl}%
    \item[Associated institution:]\dotfill\inserttheirinst%
    \item[Host address:]\dotfill\url{\inserttheirrepo}%
    \item[License:]\dotfill\inserttheirlic%
    \end{description}%
  \end{mybox}%
}

\AtBeginDocument{
  \begin{mybox}[\titlestr]
    \vskip.01\linewidth
	\centering \mdname
  \end{mybox}
  
  \begin{mybox}[\termstr]
    \begin{multicols}{2}
      \iniprtchckbxschecked  
    \end{multicols}
    \begin{description}
    \item[Website:]\dotfill\url{\insertmyurl}
    \item[Host address:]\dotfill\url{\insertmyrepo}
    \item[License:]\dotfill\insertlicense
    \item[Copyright holders:]\dotfill\insertmycpyrghthldrs
    \end{description}
  \end{mybox}
}
\AtEndDocument{%
  \begin{applicants}
    \applboxcontent%
%	\signatureline{\apnone}\signatureline{\apntwo}
	% \signatureline{\apnthr}
  \end{applicants}

  \begin{mybox}[\permistr]
    % \begin{Form}
    %   \renewcommand{\labelitemi}{\mycheckbox{}}
    %   \begin{itemize}
    %   \item  until further notice
    %   \item  but at most until \dots\dots\dots\dots
    %   \end{itemize}
    % \end{Form}
    \signatureline{Peter Benner}
  \end{mybox}
}

